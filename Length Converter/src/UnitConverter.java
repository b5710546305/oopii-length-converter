/**
 * A unit converter object
 * used to convert an amount from a unit to another unit
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.06
 */
public class UnitConverter {
	/**
	 * Convert the unit
	 * @return the converted amount in the new unit
	 */
	public double convert(double amount, Unit fromUnit, Unit toUnit){
		return (amount*fromUnit.getValue())/toUnit.getValue();
	}
	
	/**
	 * Get the array of all available units to convert
	 * @return convertible units
	 */
	public Unit[] getUnits(){
		return new Unit[] {Length.METER, Length.KILOMETER, Length.MILE, Length.FOOT, Length.WA};
	}
}

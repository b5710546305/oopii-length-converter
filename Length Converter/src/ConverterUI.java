import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

/**
 * User interface of unit conversion
 * Converts the unitw of length
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.06
 */
public class ConverterUI extends JFrame {

	/**The unit converter*/
	private UnitConverter unitconverter;
	
	/**Textfields Inputs*/
	private JTextField from;
	private JTextField to;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConverterUI frame = new ConverterUI(new UnitConverter());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructor
	 */
	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;
		
		setTitle("Length Converter");
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 639, 78);
		getContentPane().setLayout(null);
		
		//3 km = 3000 m
		//how to turn 3 into 3000?
		//System.out.print(uc.convert(3.0, Length.KILOMETER, Length.METER));
		
		initComponents();
	}
	
	/**
	 * Initialize the GUI components
	 */
	public void initComponents(){
		from = new JTextField();
		from.setBounds(10, 11, 94, 20);
		getContentPane().add(from);
		from.setColumns(10);
		
		JComboBox fromUnit = new JComboBox();
		fromUnit.setMaximumRowCount(100);
		fromUnit.setModel(new DefaultComboBoxModel(Length.values()));
		fromUnit.setBounds(114, 11, 89, 20);
		getContentPane().add(fromUnit);
		
		JLabel label = new JLabel("=");
		label.setBounds(213, 14, 25, 14);
		getContentPane().add(label);
		
		to = new JTextField();
		to.setEnabled(false);
		to.setBounds(228, 11, 94, 20);
		getContentPane().add(to);
		to.setColumns(10);
		
		JComboBox toUnit = new JComboBox();
		toUnit.setMaximumRowCount(100);
		toUnit.setModel(new DefaultComboBoxModel(Length.values()));
		toUnit.setBounds(332, 11, 89, 20);
		getContentPane().add(toUnit);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double varFrom = Double.parseDouble(from.getText());
				Unit varFromUnit = ((Unit)fromUnit.getSelectedItem());
				Unit varToUnit  = ((Unit)toUnit.getSelectedItem());
				
				double varTo = unitconverter.convert(varFrom, varFromUnit, varToUnit);
				to.setText(String.format("%.3f", varTo));
				
			}
		});
		btnNewButton.setBounds(431, 10, 89, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Clear");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				from.setText("");
				to.setText("");
			}
		});
		btnNewButton_1.setBounds(528, 10, 89, 23);
		getContentPane().add(btnNewButton_1);
	}
}
